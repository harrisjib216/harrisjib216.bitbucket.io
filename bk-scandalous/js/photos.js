$(document).ready(function () {
/*---------- side navigation button ----------*/
    $('.open').sideNav();
/*---------- end of side navigation button ----------*/
                
            
/*---------- bk slider ----------*/
    $('.slider').slider();
/*---------- end of bk slider ----------*/
                
            
/*---------- social ----------*/
// when the mouse hovers over social media - show the links
    $('.links').hide();
// hover over soical media
    $('#socialM').mouseenter(function () {
        $('.links').show(1200);
    });
// hover away
    $('.side-nav').mouseleave(function () {
        $('.links').hide('slow');
    });
    
    
// social links
    $('.hovered').mouseenter(function () {
        $(this).css('color', '#f1f1f1');
    });
    $('.hovered').mouseleave(function () {
        $(this).css('color', '#818181');
    });
/*---------- end of social ----------*/
    
    
/*---------- ----------*/
/*---------- ----------*/



/*---------- scroll back up ----------*/
// hover - color to grey
    $('#goToTop').mouseenter(function () {
        $('#goToTop').css('color', 'grey');
    });
    
// no hover - color to white
    $('#goToTop').mouseleave(function () {
        $('#goToTop').css('color', 'white');
    });
    
// pressed - scroll up
    $('#goToTop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 2500);
    });
/*---------- scroll back up ----------*/
});