$(document).ready(function () {
/*----------  side navigation ----------*/
// open side nav bar
    $(".open").sideNav();
/*---------- end of side navigation bar ----------*/



/*---------- slider ----------*/
// slider
    $('.slider').slider();
/*---------- end of slider ----------*/
    


/*---------- hover over the top links ----------*/
// hover over the top links
    $('.topLink').mouseenter(function () {
        $(this).css('color', '#f1f1f1');
    });
// hover away from the top links
    $('.topLink').mouseleave(function () {
        $(this).css('color', '#bdbdbd');
    });


// when the mouse hovers over social media - show the links
    $('.links').hide();
// hover over soical media
    $('#socialM').mouseenter(function () {
        $('.links').show(1200);
    });
// hover away
    $('.side-nav').mouseleave(function () {
        $('.links').hide('slow');
    });
    
    
// social links
    $('.hovered').mouseenter(function () {
        $(this).css('color', '#f1f1f1');
    });
    $('.hovered').mouseleave(function () {
        $(this).css('color', '#818181');
    });
/*---------- end of hover over the top links ----------*/



/*---------- about me ----------*/
// when the about me is clicked
    $('#aboutMe').click(function () {
        $('html, body').animate({
            scrollTop: ($('.aboutMe').offset().top)
        }, 2500);
    });
/*---------- end of about me ----------*/



/*---------- contact ----------*/
// when the contact is clicked
    $('#contact').click(function () {
        $('html, body').animate({
            scrollTop: ($('.contact').offset().top)
        }, 2500);
    });
/*---------- end of contact ----------*/



/*---------- scroll back up ----------*/
// hover - color to grey
    $('#goToTop').mouseenter(function () {
        $('#goToTop').css('color', 'grey');
    });
    
// no hover - color to white
    $('#goToTop').mouseleave(function () {
        $('#goToTop').css('color', 'white');
    });
    
// pressed - scroll up
    $('#goToTop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 2500);
    });
/*---------- scroll back up ----------*/
    

    

});